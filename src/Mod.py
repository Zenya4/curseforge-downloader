BASE_URL_MOD = "https://www.curseforge.com/minecraft/mc-mods"
BASE_URL_TEXTURE = "https://www.curseforge.com/minecraft/texture-packs"
BASE_URL_DOWNLOAD = "https://mediafiles.forgecdn.net/files"

class Mod:
    def __init__(self, project_id, file_id, project_url):
        self.project_id = str(project_id)
        self.file_id = str(file_id)
        self.project_url = project_url
        self.file_name = ""

        self.base_url = BASE_URL_MOD if "mc-mods" in self.project_url else BASE_URL_TEXTURE

    @property
    def project_name(self):
        return self.project_url.replace(f"{self.base_url}/", "")

    @property
    def file_url(self):
        return f"{self.base_url}/{self.project_name}/files/{self.file_id}"

    @property
    def download_url(self):
        return f"{BASE_URL_DOWNLOAD}/{self.file_id[:4]}/{str(int(self.file_id[4:]))}/{self.file_name.replace('+', '%2B')}"