import os
import sys
import zipfile
import json
import re
import time
import zipfile
import shutil
import asyncio
import aiohttp
import aiofiles
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from Mod import Mod

TEMP_DIR = os.path.join(os.getcwd(), ".cfdl")
BASE_URL_FORGE = "https://maven.minecraftforge.net/net/minecraftforge/forge"
BASE_URL_FABRIC = "https://fabricmc.net/use/installer"

mod_list = []

def throw_error(msg):
    print(f"ERROR: {msg}")
    time.sleep(5)
    sys.exit(1)

def print_banner():
    print(f"""
░█████╗░███████╗██████╗░██╗░░░░░
██╔══██╗██╔════╝██╔══██╗██║░░░░░
██║░░╚═╝█████╗░░██║░░██║██║░░░░░
██║░░██╗██╔══╝░░██║░░██║██║░░░░░
╚█████╔╝██║░░░░░██████╔╝███████╗
░╚════╝░╚═╝░░░░░╚═════╝░╚══════╝
    """)
    print(f"CurseForge Modpack Downloader by Zenya")
    print(f"Source: https://gitlab.com/Zenya4/curseforge-downloader\n")
    print(f"===========================================================")
    print(f"YOU MUST HAVE GOOGLE CHROME INSTALLED FOR THIS TOOL TO WORK")
    print(f"===========================================================\n")

def print_footer():
    print(f"Import of {modpack} complete!\n")
    print(f"Double click forge/fabric-installer.jar and install the CORRECT version specified in the file name")
    print(f"Format: <modloader>-<minecraft_version>-<modloader_version>.jar\n")
    print(f"Drag and drop all files to the install location & enjoy :)")

def request_modpack():
    print("Input the path to the modlist downloaded from CurseForge")
    print("This .zip file can be found by going to the modpack page on CurseForge -> Files -> Click on the underlined text of the modpack version you want (NOT Install) -> Download\n")
    print("Example URL: https://www.curseforge.com/minecraft/modpacks/rlcraft/files/3655670 (click 'Download')")
    print(r"Example input: 'C:\Users\yourname\Downloads\RLCraft+1.12.2+-+Release+v2.9.1c.zip' (Drag and drop works)")
    return input("> ")

def get_driver():
    opts = Options()
    # opts.add_experimental_option("prefs", {
    #     "download.default_directory": TEMP_DIR,
    #     "download.prompt_for_download": False,
    #     "download.extensions_to_open": 'jar',
    #     "safebrowsing.enabled": True
    #     })
    opts.add_argument("--safebrowsing-disable-download-protection")
    opts.add_argument("--safebrowsing-disable-extension-blacklist")
    opts.add_argument("--headless")
    opts.add_argument("--disable-gpu")
    opts.add_argument("--disable-extensions")
    opts.add_argument("--enable-javascript")
    opts.add_argument("--log-level=3")
    opts.add_argument("--disable-logging")
    opts.add_argument("user-agent=Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36")

    return webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=opts)

def populate_file_name(driver: webdriver.Chrome, mod: Mod):
    driver.get(mod.file_url)
    spans = driver.find_elements(By.TAG_NAME, "span")
    for idx, span in enumerate(spans):
        if span.text == "Filename":
            mod.file_name = spans[idx+1].text
            return
    
    print(f"Failed to get download URL for mod {mod.project_name}. Retrying...")
    driver.get(mod.project_url)
    mod.project_url = driver.current_url
    populate_file_name(driver, mod)

async def download_file(url, path):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status != 200:
                throw_error(f"Failed to download file from {url}")
            
            f = await aiofiles.open(path, mode='wb')
            await f.write(await resp.read())
            await f.close()

if __name__ == "__main__":
    print_banner()
    if len(sys.argv) != 1:
        modpack = sys.argv[1]
    else:
        modpack = request_modpack()

    if os.path.isdir(TEMP_DIR):
        print(f"Detected an existing half-completed import...")
        print(f"Would you like to continue mod download progress? (Y/n)")
        if input("> ").lower() == "n":
            os.remove(TEMP_DIR)

    if not os.path.exists(modpack):
        throw_error("Invalid path to modpack")
    
    with zipfile.ZipFile(modpack) as z:
        if "manifest.json" not in z.namelist() or "modlist.html" not in z.namelist():
            throw_error("Invalid modpack format")

        with z.open("manifest.json") as manifest:
            with z.open("modlist.html") as modlist:
                manifest_json = json.loads(manifest.read())
                mc_ver = manifest_json['minecraft']['version']
                modloader = manifest_json['minecraft']['modLoaders'][0]['id'].split("-")[0]
                modloader_ver = manifest_json['minecraft']['modLoaders'][0]['id'].split("-")[-1]
                moddata_1 = manifest_json['files']
                moddata_2 = re.findall(r'"(.*)"', modlist.read().decode())
        
        for i in range(len(moddata_2)):
            mod_list.append(Mod(project_id=moddata_1[i]['projectID'], file_id=moddata_1[i]['fileID'], project_url=moddata_2[i]))

    progress = -1
    if not os.path.isdir(os.path.join(TEMP_DIR, "mods")):
        os.makedirs(os.path.join(TEMP_DIR, "mods"))
    if os.path.isfile(os.path.join(TEMP_DIR, ".dlprogress")):
        with open(os.path.join(TEMP_DIR, ".dlprogress"), "r") as f:
            progress = int(f.read())

    driver = get_driver()
    for idx, mod in enumerate(mod_list):
        if idx <= progress:
            continue

        populate_file_name(driver, mod)  
        if sys.platform == "win32":
            asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy()) 
        asyncio.run(download_file(mod.download_url, os.path.join(TEMP_DIR, "mods", mod.download_url.split("/")[-1])))
        with open(os.path.join(TEMP_DIR, ".dlprogress"), mode='w') as f:
            f.write(str(idx))
        print(f"Successfully downloaded {mod.file_name} ({idx+1}/{len(mod_list)})")

    if modloader == "forge":
        modloader_url = f"{BASE_URL_FORGE}/{mc_ver}-{modloader_ver}/{modloader}-{mc_ver}-{modloader_ver}-installer.jar"
    elif modloader == "fabric":
        driver.get(BASE_URL_FABRIC)
        driver.refresh()
        _as = driver.find_elements(By.TAG_NAME, "a")
        for a in _as:
            print(a.text)
            if a.text == "Download universal jar":
                modloader_url = a.get_attribute("href")
    else:
        throw_error(f"Unrecognised modloader {modloader}")

    print(f"\nDownloading {modloader}...")
    if sys.platform == "win32":
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy()) 
    asyncio.run(download_file(modloader_url, os.path.join(TEMP_DIR, f"{modloader}-{mc_ver}-{modloader_ver}-installer.jar")))
    print(f"Successfully downloaded {modloader}-{mc_ver}-{modloader_ver}-installer.jar\n")


    FINAL_DIR = os.path.join(os.getcwd(), modpack.split(".zip")[0])
    print(f"Copying config overrides...")
    with zipfile.ZipFile(modpack, 'r') as z:
        z.extractall(modpack.split(".zip")[0])

    if not os.path.isfile(os.path.join(FINAL_DIR, "resourcepacks")):
        os.makedirs(os.path.join(FINAL_DIR, "resourcepacks"))
    
    temp = os.listdir(os.path.join(TEMP_DIR))
    _ = [shutil.move(os.path.join(TEMP_DIR, f), os.path.join(FINAL_DIR, f)) for f in temp]
    overrides = os.listdir(os.path.join(FINAL_DIR, "overrides"))
    _ = [shutil.move(os.path.join(FINAL_DIR, "overrides", o), os.path.join(FINAL_DIR, o)) for o in overrides]
    mods = os.listdir(os.path.join(FINAL_DIR, "mods"))    
    _ = [shutil.move(os.path.join(FINAL_DIR, "mods", t), os.path.join(FINAL_DIR, "resourcepacks", t)) for t in mods if t.endswith(".zip")]
    os.rmdir(os.path.join(FINAL_DIR, "overrides"))
    os.remove(os.path.join(FINAL_DIR, ".dlprogress"))
    os.rmdir(TEMP_DIR)
    print_footer()
    print(f"Modpack location: {FINAL_DIR}")