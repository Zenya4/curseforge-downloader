```
░█████╗░███████╗██████╗░██╗░░░░░
██╔══██╗██╔════╝██╔══██╗██║░░░░░
██║░░╚═╝█████╗░░██║░░██║██║░░░░░
██║░░██╗██╔══╝░░██║░░██║██║░░░░░
╚█████╔╝██║░░░░░██████╔╝███████╗
░╚════╝░╚═╝░░░░░╚═════╝░╚══════╝
```
CurseForge Modpack Downloader by Zenya
<br>
Source: https://gitlab.com/Zenya4/curseforge-downloader
<br>
<br>
YOU MUST HAVE GOOGLE CHROME INSTALLED FOR THIS TOOL TO WORK
<br>
<br>
**Overview**
<br>
CurseForge recently started limiting its API access such that you can only download modpacks using Overwolf's official launcher. This was done in order to increase popularity for their official launcher so that they can earn more ad revenue.

Of course, this change sucked for many modpack creators and players since most of us would like to have the option to use other launchers (like MultiMC or PolyMC).

As such, I have created this tool to unofficially download modpacks via web scraping. You can input a "modpack data" [(example)](https://www.curseforge.com/minecraft/modpacks/rlcraft/files/3655670) file which is still available for download from CurseForge, and this tool will parse and install all required mods from the data file.
<br>

**Requirements**
- Gooogle Chrome
- Python 3.10.5
<br>

**Usage**
1. Download [Google Chrome](https://www.google.com/chrome/)
2. Clone the repository
3. Make sure you are in the correct directory `cd curseforge-downloader-master`
4. Install the python requirements `pip install -r requirements.txt`
5. Run `python3 src\install.py`
6. Wait for the downloads to complete
7. In the modpack folder, run the forge/fabric-installer.jar file and install the correct version corresponding to the modpack (see `manifest.json`)
8. Drag and drop your modpack files to the modloader install location
9. Enjoy your modpack!
<br>

**Features**
- Bypasses Cloudflare when downloading your mods
- Pure web-scraping; No CurseForge API key is used so it cannot be revoked
- Asynchronous mod downloads
- Works with both Forge and Fabric modpacks
<br>

Working as of 20/07/2022
